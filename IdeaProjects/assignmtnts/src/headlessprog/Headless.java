package headlessprog;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Headless {


    public static WebDriver driver;

    public static void main(String s[]) throws IOException {


        WebDriverManager.chromedriver().setup();

        ChromeOptions s1 = new ChromeOptions();
        s1.addArguments("--headless");

        driver = new ChromeDriver(s1);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.geeksforgeeks.org/");
        WebElement e = driver.findElement(By.linkText("Data Structures"));
        e.click();



        WebElement inputBox = driver.findElement(By.cssSelector(".gLFyf.gsfi"));
        inputBox.sendKeys("TestVagrant"+ Keys.RETURN);


        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("screenshot3.png"));
    }

}




